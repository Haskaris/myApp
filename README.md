# TODO List Project

Simple application de gestion de liste de Todo faite par Mathis Perrier et Charly Vinson.


## Connexion
Pour pouvoir acceder à l'application il est nécessaire d'avoir un compte (le compte n'a pas besoin d'être validé/vérifié pour autant).
Pour ce faire il vous faudra créer un compte grâce à une adresse mail et un mot de passe.
L'adresse mail n'a pas besoin d'être valide, mais cela implique que vous ne serez pas en capacité de recevoir un mail si vous perdez votre mot de passe.
Un mail de confirmation sera envoyé lors de l'inscription.

## Principe de l'application
Un utilisateur va pouvoir créer et partager des listes, pourra rejoindre des listes en tant qu'écrivain ou lecteur. Tout ceci est fait en temps réel, si quelqu'un modifie la liste alors que vous êtes dessus, vous verrez la liste se modifier en conséquence.

Si l'utilisateur possède la liste, en plus des droits d'écriture il pourra modifier la liste (ie: le titre, la photo) et gérer les paramètres de partage
Sur une liste avec un droit d'écriture, en plus des droits de lecture il pourra ajouter, supprimer et modifier (cocher) les todos.
Sur une liste avec un droit de lecture uniquement, il pourra simplement regarder l'état d'avancement de la liste.

### Roles éxistant
- owner
- writer
- reader

## Spécificité

### Natif
#### Photo

Comme évoqué dans le [principe de l'application](#principe-de-lapplication), une liste peut avoir une photo de présentation. Cette photo peut directement être prise par l'appareil photo du téléphone ou via l'album de celui-ci.

Une fois publié elle sera stockée dans firestorage. Si vous changez l'image de votre liste, l'image précédente sera supprimée.

### Navigateur
#### Authentification Google

Il est possible de se connecter grâce à l'authentification google depuis un navigateur (testé sur ordinateur). Cependant par manque de temps nous n'avons pas ajouté la fonctionnalité native.
