import { Injectable } from '@angular/core';
import { List } from '../models/list';
import { Todo } from '../models/todo';
import firebase from 'firebase/compat/app'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(
    private afs: AngularFirestore,
  ) { }

  //// CRUD section ///////////////////////////////////////////////////////////
  addTodoToList(listId: string, todo: Todo) {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)
    let tDoc = lDoc.collection<Todo>('todos').doc()
    tDoc.set({
      id: tDoc.ref.id,
      name: todo.name,
      description: todo.description,
      isDone: todo.isDone,
    })
  }

  getAllTodos(listId: string): Observable<Todo[]> {
    return this.afs.collection<List>('playlists').doc(listId).collection<Todo>('todos').valueChanges({idField:'id'})
  }

  updateTodoIsDone(listId: string, todoId: string, isDone: boolean): void {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)
    let tDoc = lDoc.collection('todos').doc(todoId)
    tDoc.update({
      isDone: isDone
    })
  }
  
  removeTodoToList(listId: string, todoId: string): Promise<void> {
    return this.afs.collection('playlists').doc(listId).collection('todos').doc(todoId).delete()
  }
}
