import { Injectable } from '@angular/core';
import { List } from '../models/list';
import { Todo } from '../models/todo';
import firebase from 'firebase/compat/app'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { TodoService } from './todo.service';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  listsCollection_owner: AngularFirestoreCollection<List>;
  listsCollection_reader: AngularFirestoreCollection<List>;
  listsCollection_writer: AngularFirestoreCollection<List>;

  constructor(
    private afs: AngularFirestore,
    private afs_storage: AngularFireStorage,
    private todoService: TodoService
  ) {}

  //// utils section //////////////////////////////////////////////////////////

  loadPlaylists() {
    const userEmail = firebase.auth().currentUser.email
    
    this.listsCollection_owner = this.afs.collection<List>('playlists',
      ref => ref.where(
        "owner", "==", userEmail
      )
    )
    this.listsCollection_reader = this.afs.collection<List>('playlists',
      ref => ref.where(
        "readers", "array-contains", userEmail
      )
    )
    this.listsCollection_writer = this.afs.collection<List>('playlists',
      ref => ref.where(
        "writers", "array-contains", userEmail
      )
    )
  }

  getAllList(): Observable<List[]> {
    const owner$ = this.listsCollection_owner.valueChanges({idField:'id'}).pipe(
      map(lists => lists.map(list => ({...list, right:'owner'} as List)))
    )
    const reader$ = this.listsCollection_reader.valueChanges({idField:'id'}).pipe(
      map(lists => lists.map(list => ({...list, right:'reader'} as List)))
    )
    const writer$ = this.listsCollection_writer.valueChanges({idField:'id'}).pipe(
      map(lists => lists.map(list => ({...list, right:'writer'} as List)))
    )
    
    return combineLatest([owner$, reader$, writer$]).pipe(
      map(([owner, reader, writer]) => owner.concat(reader).concat(writer))
    )
  }

  getRightForList(list: List): 'owner' | 'writer' | 'reader' {
    const user: firebase.User = firebase.auth().currentUser
    if (user) {
      const email: string = user.email
      
      // writers.includes provoque une erreur lors de la suppression de la playlist
      return list.owner === email ? 'owner' : list.writers.includes(email) ? 'writer' : 'reader'
    }

    return 'reader'
  }

  getOneList(id: string): Observable<List> {
    return this.afs.collection<List>('playlists').doc(id).valueChanges({idField:'id'}).pipe(
      map(list => ({...list, right: this.getRightForList(list)}))
    )
  }

  getListImage(listId: string): Observable<any> {
    const ref = this.afs_storage.ref(`images/${listId}`);
    return ref.getDownloadURL()
  }
  
  createList(name: string, description?: string, todos?: Todo[], image?: string): void {
    const list = new List(name, firebase.auth().currentUser.email)
    list.description = description

    this.uploadList(list)
    
    if (todos) {
      todos.forEach(todo => {
        this.todoService.addTodoToList(list.id, todo)
      })
    }
    
    if (image) {
      this.uploadListImage(list.id, image)
    }

    
    
  }
  
  uploadList(list: List) {
    let lDoc = this.afs.collection<List>('playlists').doc()
    lDoc.set({
      id: lDoc.ref.id,
      description: list.description,
      name: list.name,
      writers: list.writers,
      readers: list.readers,
      owner: list.owner,
    })
  }
  
  
  updateList(id: string, name?: string, description?: string, image?: string): void {
    if (name) {
      this.updateListName(id, name)
    }
    if (description) {
      this.updateListDescription(id, description)
    }
    if (image) {
      this.uploadListImage(id, image)
    }
  }
  
  updateListDescription(id: string, description: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(id)
    lDoc.update({
      description: description
    })
  }

  uploadListImage(id: string, image: string): void {
    const ref = this.afs_storage.ref(`images/${id}`)
    ref.putString(image, 'data_url')
  }
  
  updateListName(id: string, name: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(id)
    lDoc.update({
      name: name
    })
  }

  addWriterToList(listId: string, userEmail: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)

    this.removeReaderFromList(listId, userEmail)

    lDoc.ref.get().then(doc => {
      let list: List = doc.data()
      if (list.writers.indexOf(userEmail) == -1) {
        list.writers.push(userEmail)
        lDoc.update({
          writers: list.writers
        })
      }
    })
  }

  addReaderToList(listId: string, userEmail: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)

    this.removeWriterFromList(listId, userEmail)

    lDoc.ref.get().then(doc => {
      let list: List = doc.data() as List
      if (list.readers.indexOf(userEmail) == -1) {
        list.readers.push(userEmail)
        lDoc.update({
          readers: list.readers
        })
      }
    })
  }

  removeWriterFromList(listId: string, userEmail: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)
    lDoc.ref.get().then(doc => {
      let list: List = doc.data()
      let writers: string[] = list.writers
      writers = writers.filter(writer => writer !== userEmail)
      if (writers.length != list.writers.length) {
        lDoc.update({
          writers: writers
        })
      }
    })
  }

  removeReaderFromList(listId: string, userEmail: string): void {
    let lDoc = this.afs.collection<List>('playlists').doc(listId)
    lDoc.ref.get().then(doc => {
      let list: List = doc.data()
      let readers: string[] = list.readers
      readers = readers.filter(reader => reader !== userEmail)
      if (readers.length != list.readers.length) {
        lDoc.update({
          readers: readers
        })
      }
    })
  }

  deleteList(listId: string) {
    this.todoService.getAllTodos(listId).pipe(first()).subscribe(async (todos) => {
      await Promise.all(
        todos.map(todo => {
          return this.todoService.removeTodoToList(listId, todo.id);
        })
      );

      this.afs.collection('playlists').doc(listId).delete().then(() => {
        console.log("Playlists successfully deleted!");
      }).catch((error) => {
        console.error("Error removing document: ", error);
      })
    })
  }
}
