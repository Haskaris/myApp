import { Pipe, PipeTransform } from '@angular/core';
import { List } from '../models/list';
import { Todo } from '../models/todo';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  compareListByRightAndName(a: List, b: List): number {
    if (a.right === b.right) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    } else {
      if (a.right === 'owner') {
        return -1;
      }
      if (b.right === 'owner') {
        return 1;
      }
      if (a.right === 'writer') {
        return -1;
      }
      if (b.right === 'writer') {
        return 1;
      }
      return 0;
    }
  }

  compareTodoByIsDoneAndName(a: Todo, b: Todo): number {
    if (a.isDone === b.isDone) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    } else {
      if (a.isDone < b.isDone) {
        return -1;
      }
      if (a.isDone > b.isDone) {
        return 1;
      }
    }
  }

  transform(values: any[], order: 'asc' | 'desc', type: 'List' | 'Todo'): any[] {
    if (!values || !order || !type) {
      return values   // no array or no order or no column
    }

    if (values.length <= 1) {
      return values   // array with only one item
    }

    if (type == 'Todo') {
      return values.sort(this.compareTodoByIsDoneAndName);
    } else if (type === 'List') {
      return values.sort(this.compareListByRightAndName);
    }
  }
}
