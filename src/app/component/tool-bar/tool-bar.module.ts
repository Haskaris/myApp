import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolBarComponent } from './tool-bar.component';
import { ToolBarRoutingModule } from './tool-bar-rooting.module';

@NgModule({
	imports: [
		CommonModule,
		ToolBarRoutingModule
	],
	declarations: [ToolBarComponent],
	exports: [ToolBarComponent]
})
export class ToolBarModule {}
