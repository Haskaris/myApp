import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NavController } from '@ionic/angular';
import firebase from 'firebase/compat/app';

@Component({
  selector: 'tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss'],
})
export class ToolBarComponent implements OnInit {
  
  @Input() name?: string = '';
  @Input() displayHome?: boolean = true;
  @Input() displayLogInOut?: boolean = true;

  isConnected: boolean = false;

  constructor(
    private auth: AngularFireAuth,
    private nav: NavController
  ) {}

  ngOnInit() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isConnected = true
      } else {
        this.isConnected = false
      }
    });
  }

  logout(): void {
    this.auth.signOut().then(
      () => {
        this.nav.navigateRoot('/login')
      }
    )
  }

  login(): void {
    this.nav.navigateRoot('/login')
  }

  goBack(): void {
    this.nav.back()
  }

}
