import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import firebase from 'firebase/compat/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentialsForm: FormGroup
  errorCode: string = ''

  constructor(
    private formBuilder: FormBuilder,
    public auth: AngularFireAuth,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.credentialsForm = this.createFormGroup()
  }
  
  createFormGroup() {
    return this.formBuilder.group({
      email: [
        '', 
        [
          Validators.required,
          Validators.email,
        ],
      ],
      password: [
        '', 
        [
          Validators.required,
          Validators.minLength(6),
        ],
      ],
    })
  }

  onEmailChange(event): void {
    this.errorCode = ''
  }

  onPasswordChange(event): void {
    this.errorCode = ''
  }

  login() {
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        this.nav.navigateForward('/home')
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage)
      })
  }

  submitLogin() {
    const email = this.credentialsForm.value.email
    const password = this.credentialsForm.value.password
    this.auth.signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        this.nav.navigateForward('/home')
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode)
        this.errorCode = errorCode
      });
  }

  logout() {
    this.auth.signOut();
  }

}
