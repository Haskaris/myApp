import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgottenPasswordPageRoutingModule } from './forgotten-password-routing.module';

import { ForgottenPasswordPage } from './forgotten-password.page';
import { ToolBarModule } from 'src/app/component/tool-bar/tool-bar.module';

@NgModule({
  imports: [
    CommonModule,
    ForgottenPasswordPageRoutingModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ToolBarModule
  ],
  declarations: [ForgottenPasswordPage]
})
export class ForgottenPasswordPageModule {}
