import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.page.html',
  styleUrls: ['./forgotten-password.page.scss'],
})
export class ForgottenPasswordPage implements OnInit {
  emailForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public auth: AngularFireAuth,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.emailForm = this.createFormGroup()
  }

  createFormGroup() {
    return this.formBuilder.group({
      email: [
        '', 
        [
          Validators.required,
          Validators.email,
        ],
      ],
    })
  }

  submit() {
    const email = this.emailForm.value.email
    this.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.nav.navigateForward('/home')
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error)
        this.nav.navigateForward('/home')
      });
  }

}
