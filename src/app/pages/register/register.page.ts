import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import firebase from 'firebase/compat/app';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  credentialsForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public auth: AngularFireAuth,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.credentialsForm = this.createFormGroup()
  }
  
  createFormGroup() {
    return this.formBuilder.group({
      email: [
        '', 
        [
          Validators.required,
          Validators.email,
        ],
      ],
      password: [
        '', 
        [
          Validators.required,
          Validators.minLength(6),
        ],
      ],
      passwordConfirmation: [
        '', 
        [
          Validators.required,
          Validators.minLength(6),
        ],
      ],
    })
  }

  submitRegister() {
    const email = this.credentialsForm.value.email
    const password = this.credentialsForm.value.password
    const passwordConfirmation = this.credentialsForm.value.passwordConfirmation
    if (passwordConfirmation === password) {
      // const userCredential = await this.auth.createUserWithEmailAndPassword(email, password)
      // await userCredential.user.sendEmailVerification()
      // await this.auth.signOut()

      
      // faire un auth service


      this.auth.createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // On est connecté
        userCredential.user.sendEmailVerification()
        this.nav.navigateForward('/home')
      })
      .catch((error) => {
        var errorCode = error.code
        var errorMessage = error.message
        if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }
        console.log(error)
      });
    }
  }

}
