import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { List } from 'src/app/models/list';
import { Todo } from 'src/app/models/todo';
import { ListService } from 'src/app/services/list.service';
import { TodoService } from 'src/app/services/todo.service';
import { CreateTodoComponent } from 'src/app/modals/create-todo/create-todo.component';
import { EditListComponent } from 'src/app/modals/edit-list/edit-list.component';
import { ShareListComponent } from 'src/app/modals/share-list/share-list.component';

@Component({
  selector: 'app-list-details',
  templateUrl: 'list-details.page.html',
  styleUrls: ['list-details.page.scss'],
})
export class ListDetailsPage implements OnInit {

  list$: Observable<List>
  todos$: Observable<Todo[]>

  displayObservable: boolean = true
  isEditDescriptionAllowed: boolean = false
  image: string
  image$: Observable<any>
  id: string

  constructor(
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private listService: ListService,
    private todoService: TodoService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public afs: AngularFireStorage
  ) { }

  // Display the modal
  // We pass the modal controller to allow the component to close himself
  async presentTodoModal() {
    const modal = await this.modalController.create({
      component: CreateTodoComponent,
      cssClass: 'app-create-todo',
      componentProps: {
        'modalController': this.modalController,
        'listId': this.id
      }
    })
    return await modal.present()
  }

  async presentEditModal() {
    const modal = await this.modalController.create({
      component: EditListComponent,
      cssClass: 'app-edit-list',
      componentProps: {
        'modalController': this.modalController,
        'listId': this.id,
        'list$': this.list$
      }
    })

    // dismiss modal with data
    modal.onDidDismiss().then(data => {
      this.image = data.data['image']
      this.displayObservable = false
    })

    return await modal.present()
  }

  async presentShareModal() {
    const modal = await this.modalController.create({
      component: ShareListComponent,
      cssClass: 'app-share-list',
      componentProps: {
        'modalController': this.modalController,
        'listId': this.id,
      }
    })
    return await modal.present()
  }
  
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id']

    this.list$ = this.listService.getOneList(this.id)
    this.todos$ = this.todoService.getAllTodos(this.id)
    this.image$ = this.listService.getListImage(this.id)
  }
  
  allowEditDescription() {
    this.isEditDescriptionAllowed = !this.isEditDescriptionAllowed
  }

  updateDescription(event) {
    this.listService.updateListDescription(this.id, event.target.value);

    this.isEditDescriptionAllowed = false
  }

  todoChecked(todo: Todo): void {
    this.todoService.updateTodoIsDone(this.id, todo.id, !todo.isDone)
  }

  deleteList(list: List) {
    this.listService.deleteList(list.id)
  }

  deleteTodo(todo: Todo) {
    this.todoService.removeTodoToList(this.id, todo.id)
  }
}
