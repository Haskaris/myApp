import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListDetailsPage } from './list-details.page';

import { ListDetailsPageRoutingModule } from './list-details-routing.module';
import { CreateTodoComponent } from 'src/app/modals/create-todo/create-todo.component';
import { ToolBarModule } from 'src/app/component/tool-bar/tool-bar.module';
import { EditListComponent } from 'src/app/modals/edit-list/edit-list.component';
import { SortByPipe } from 'src/app/pipes/sort-by.pipe';
import { ShareListComponent } from 'src/app/modals/share-list/share-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListDetailsPageRoutingModule,
    ReactiveFormsModule,
    ToolBarModule,
  ],
  declarations: [
    CreateTodoComponent,
    EditListComponent,
    ListDetailsPage,
    ShareListComponent,
    SortByPipe,
  ]
})
export class ListDetailsPageModule {}
