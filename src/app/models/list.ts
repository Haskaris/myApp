export class List {
	id: string;
	description: string = '';
	name: string = '';
	right?: 'writer' | 'reader' | 'owner';

	writers: string[] = [];
	readers: string[] = [];
	owner: string;

	constructor(n: string, user: string) {
		this.name = n;
		this.owner = user;
	}
}
