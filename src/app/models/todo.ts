export class Todo {
	id: string;
	name: string = '';
	description: string = '';
	isDone: boolean = false;

	constructor(n: string, d: string = '', isdone: boolean = false) {
		this.name = n;
		this.description = d;
		this.isDone = isdone;
	}
}
