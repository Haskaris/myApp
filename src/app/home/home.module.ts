import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { CreateListComponent } from '../modals/create-list/create-list.component';
import { ToolBarModule } from '../component/tool-bar/tool-bar.module';
import { SortByPipe } from 'src/app/pipes/sort-by.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ReactiveFormsModule,
    ToolBarModule
  ],
  declarations: [
    CreateListComponent,
    HomePage,
    SortByPipe,
  ]
})
export class HomePageModule {}
