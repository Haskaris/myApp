import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CreateListComponent } from 'src/app/modals/create-list/create-list.component';
import { List } from 'src/app/models/list';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  lists$: Observable<List[]>;

  isModalOpen: boolean = false;

  constructor(
    private modalController: ModalController,
    private listService: ListService,
    private nav: NavController
  ) { }
  
  ngOnInit() {
    this.listService.loadPlaylists()
    this.lists$ = this.listService.getAllList()
  }

  // Display the modal
  // We pass the modal controller to allow the component to close himself
  async presentModal_newList() {
    const modal = await this.modalController.create({
      component: CreateListComponent,
      cssClass: 'app-create-list',
      componentProps: {
        'modalController': this.modalController
      }
    });
    return await modal.present();
  }

  showDetails(list: List) {
    this.nav.navigateForward('/detail/' + list.id)
  }

}
