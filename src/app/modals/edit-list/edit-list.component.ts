import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { List } from 'src/app/models/list';
import { CameraService } from 'src/app/services/camera.service';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss'],
})
export class EditListComponent implements OnInit {

  @Input() modalController: ModalController
  @Input() list$: Observable<List>
  @Input() listId: string
  
  listEditForm: FormGroup
  image: string
  image$: Observable<any>
  displayObservable: boolean = true

  constructor(
    private formBuilder: FormBuilder,
    private listService: ListService,
    private cameraService: CameraService
  ) { }
  
  //// Cycle of life //////////////////////////////////////////////////////////
  ngOnInit() {
    this.listEditForm = this.createFormGroup()
    this.list$.subscribe(list => {

      this.listEditForm.patchValue({
        name: list.name,
        description: list.description
      })
    })

    this.image$ = this.listService.getListImage(this.listId)
    this.displayObservable = true
  }

  ngOnDestroy() {
    this.listEditForm.reset()
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(5)
        ])
      ],
      description: ['']
    })   
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true,
      'image': this.image
    })
  }

  submit() {
    this.listService.updateList(this.listId, this.listEditForm.value.name, this.listEditForm.value.description, this.image)

    this.dismiss()
  }

  //// Photo section ////////

  async addPhoto(type: string): Promise<void> {
    var imagetmp: Promise<any>
    if( type === "camera") {
      imagetmp = await this.cameraService.openCamera()
    } else {
      imagetmp = await this.cameraService.openLibrary()
    }
    this.image = 'data:image/jpg;base64,' + imagetmp
    this.displayObservable = false
  }

}
