import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { List } from 'src/app/models/list';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-share-list',
  templateUrl: './share-list.component.html',
  styleUrls: ['./share-list.component.scss'],
})
export class ShareListComponent implements OnInit {
  @Input() modalController: ModalController
  @Input() listId: string

  shareListForm: FormGroup
  list$: Observable<List>
  
  constructor(
    private formBuilder: FormBuilder,
    private listService: ListService,
    private auth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.shareListForm = this.createFormGroup()
    this.list$ = this.listService.getOneList(this.listId)
  }

  ngOnDestroy() {
    this.shareListForm.reset()
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ],
      right: [
        '',
        Validators.compose([
          Validators.required
        ])
      ]
    })
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  submit() {
    // if the email form is valid
    if (this.shareListForm.valid) {
      // if the email value is different from the current user
      this.auth.currentUser.then(user => {
        if (user.email !== this.shareListForm.value.email) {
          if (this.shareListForm.value.right === 'writer') {
            this.listService.addWriterToList(this.listId, this.shareListForm.value.email)
          } else if (this.shareListForm.value.right === 'reader') {
            this.listService.addReaderToList(this.listId, this.shareListForm.value.email)
          }
        }
      })
    }

    this.dismiss()
  }

  removeWriter(email: string): void {
    this.listService.removeWriterFromList(this.listId, email)
  }

  removeReader(email: string): void {
    this.listService.removeReaderFromList(this.listId, email)
  }
}
