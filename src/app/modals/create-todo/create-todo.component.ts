import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { List } from 'src/app/models/list';
import { Todo } from 'src/app/models/todo';
import { ListService } from 'src/app/services/list.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss'],
})
export class CreateTodoComponent implements OnInit {

  @Input() modalController: ModalController
  @Input() listId: string

  todoForm: FormGroup
  
  constructor(
    private formBuilder: FormBuilder,
    private listService: ListService,
    private todoService: TodoService
  ) {
    this.todoForm = this.createFormGroup()
  }

  ngOnInit() {}

  createFormGroup() {
    return this.formBuilder.group({
      name: [
        '', 
        [
          Validators.required,
          Validators.minLength(3),
        ],
      ]
    })
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  submit() {
    this.todoService.addTodoToList(
      this.listId,
      new Todo(this.todoForm.value.name)
    )

    this.dismiss()
  }
}
