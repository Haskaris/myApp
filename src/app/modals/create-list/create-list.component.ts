import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ListService } from 'src/app/services/list.service';
import { Observable } from 'rxjs';
import { CameraService } from 'src/app/services/camera.service';

@Component({
  selector: 'app-create-list',
  templateUrl: './create-list.component.html',
  styleUrls: ['./create-list.component.scss'],
})
export class CreateListComponent implements OnInit {
  
  @Input() modalController: ModalController
  
  listForm: FormGroup
  image: string
  image$: Observable<any>
  displayObservable: boolean = true

  constructor(
    private formBuilder: FormBuilder,
    private listService: ListService,
    private cameraService: CameraService
  ) {
    this.listForm = this.createFormGroup()
  }

  ngOnInit() {
    this.displayObservable = true
  }

  createFormGroup() {
    return this.formBuilder.group({
      name: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(5)
        ])
      ],
      description: ['']
    })   
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  submit() {
    this.listService.createList(this.listForm.value.name, this.listForm.value.description, [], this.image)

    this.dismiss()
  }

  //// Photo section ////////

  async addPhoto(type): Promise<void> {
    var imagetmp: Promise<any>
    if( type == "camera") {
      imagetmp = await this.cameraService.openCamera()
    } else {
      imagetmp = await this.cameraService.openLibrary()
    }
    this.image = 'data:image/jpg;base64,' + imagetmp
    this.displayObservable = false
  }

  async uploadFirebase() {
    // TESTER SANS
    // const loading = await this.loadingController.create({
    //   duration: 2000
    // });
    // await loading.present();
    
    // const id = this.activatedRoute.snapshot.params['id']
    // const file = this.image;
    // const filePath = `images/${id}`;
    // const ref = this.afs.ref(filePath);
    // const task = ref.putString(file, 'data_url');
  }
}
